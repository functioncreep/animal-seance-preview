// Animal/Seance description, copyright, etc etc.
//
// TODO: finish animal selection functions (data selection)
// TODO: Add question-page advancing functions
// TODO: fix timing of navDisplay button fades
// TODO: put limitations on keyboard nav depending on page
// TODO: add in animal data in question placeholders

var questions;
var answers;

$(document).ready(function () {
    var container, allPages, page, animal, animalsDB, selectedAnimal, questionIndex;

    container = $('.container');
    allPages = $('.container .page');
    page = 1;

    answers = ['', '', '', '', '', '', ''];

    // fade in/out transitions
    function fadeTransition (target, loadFunc) {
    // check if target is hidden already
        if (target.is(':hidden')) {
            if (loadFunc) loadFunc();
            target.fadeIn(1000);
        } else {
            target.fadeOut(1000, function () {
            // allow function to be called between fades
                if (loadFunc) loadFunc();
                target.fadeIn(1000);
            });
        }
    }


    // Fade navbar in or out depending on page
    function navDisplay () {

        var currentPage = $('.container .page:nth-child(' + page + ')');
        var navBar = $('#navbar');

        console.log(page);

        if (currentPage.attr('navbar') === 'both') {
            console.log(currentPage.attr('navbar'));
            if (navBar.is(':hidden')) {
                $('#navbar .next-button').show();
                $('#navbar .prev-button').show();
                navBar.fadeIn(1000);
            } else {
                $('#navbar .prev-button:hidden').fadeIn(1000);
                $('#navbar .next-button:hidden').fadeIn(1000);
            }

        } else if (currentPage.attr('navbar') === 'prev') {
            console.log(currentPage.attr('navbar'));
            if (navBar.is(':hidden')) {
                $('#navbar .next-button').hide();
                navBar.fadeIn(1000);
            } else {
                $('#navbar .next-button').fadeOut(1000);
            }

        } else if (currentPage.attr('navbar') === 'next') {
            console.log(currentPage.attr('navbar'));
            if (navBar.is(':hidden')) {
                $('#navbar .prev-button').hide();
                navBar.fadeIn(1000);
            } else {
                 $('#navbar .prev-button').fadeOut(1000);
            }

        } else {
            navBar.fadeOut(1000);
            console.log('none');
        };

        if (currentPage.children('#question-page').length &&
            $('#navbar .submit-button').is(':hidden')) {

            $('#navbar .next-button').fadeOut(1000, function() {
                // $('#navbar .next-button h1').text('Submit');
                $('#navbar .next-button').hide();
                $('#navbar .submit-button').fadeIn(1000);
            });

        } else if (currentPage.children('#question-page').length === 0 &&
                   $('#navbar .submit-button').is(':visible')) {

            $('#navbar .submit-button').fadeOut(1000, function() {
            //    $('#navbar .next-button h1').text('>');
                $('#navbar .submit-button').hide()
                // $('#navbar .next-button').fadeIn(1000);
                navDisplay();
           });
        }
    }


    function pageFlip (direction) {

        function getPage () {
            return $('.container .page:nth-child(' + page + ')');
        };

        // "flip" pages depending on direction
        if (direction === "forward") {
            fadeTransition($('#viewer'), function() {
                getPage().hide();
                page ++;
                navDisplay();
                getPage().show();
            });
        } else if (direction === "back") {
            fadeTransition($('#viewer'), function() {
                getPage().hide();
                page --;
                navDisplay();
                getPage().show();
            });
        }
    };
    

    function navigation () {

        function keyboardNav () {
          // keyboard navigation...
          // TODO: enable / disable based on page in index.html
          document.addEventListener('keydown', function (event) {
            // forward key
            if (event.code === 'ArrowRight') {
              if ($('.container .page:nth-child(' + (page + 1) + ')').length !== 0) {
                pageFlip('forward');
              }
            // back button:
            } else if (event.code === 'ArrowLeft') {
              pageFlip('back');
            }
          });
        };


        function animalSelection (animal) {
            selectedAnimal = animalsDB[animal];

            // animal data query to go here
            console.log(selectedAnimal);
            questionIndex = 0;

            questions = [
                'What was ' + animal + '\'s age (in human years) at death?',
                'What was ' + animal + '\'s biological sex?',
                animal + '\'s temperament?',
                'What was ' + animal + '\'s favourite toy?',
                'What was ' + animal + '\'s favourite activity?',
                animal + '\'s favourite food?',
                'What was the cause of ' + animal + '\'s passing?'
            ];

            $('#question').text(questions[0]);
            $('#response-text').val(answers[0]);
            $("#question-page #animal-image h1").text(animal);
            $('#question-page #animal-image img').attr('src', selectedAnimal['image']);
        };


        function prevButton   () {
            var button = $('.prev-button');
            var currentPage = $('.container .page:nth-child(' + page + ')');
            var questionPage = $('#questionnaire');
            var response = $('#response-text');
            var question = $('#question');
            var submit = $('.submit-button');

            button.on('click', function () {
                if (questionIndex > 0) {

                    questionPage.fadeOut(1000, function () {
                        questionIndex --;
                        submit.css('opacity', '1');
                        question.text(questions[questionIndex]);
                        response.val(answers[questionIndex]);
                        questionPage.fadeIn(1000);
                    });
                } else {
                    pageFlip('back');
                }
            });
        };


        function nextButton() {
            var button = $('.next-button');

            button.on('click', function () {
            // check if animal-selection button, if so get animal data
                if (this.id === 'animal-selection') {
                    var selectedAnimal = $(this).children('h3').text();

                    if (selectedAnimal !== animal) {
                        answers = ['', '', '', '', '', '', ''];
                        animal = selectedAnimal;
                    };

                    animalSelection(animal);
                };

                if ($('.container .page:nth-child(' + (page + 1) + ')').length !== 0) {
                    pageFlip('forward');
                };
            });
        };

        keyboardNav();
        prevButton();
        nextButton();
    }

    // ----- Question page operations ----- //

    function submitResponse () {
        var response = $('#response-text');
        var submit = $('.submit-button');
        var questionPage = $('#questionnaire');
        var question = $('#question');

        response.defaultValue = '';
        response.on('input', function () {
            if (response.val()) {
                submit.css('opacity', '1');
            } else {
                submit.css('opacity', '0.15');
            }
        });

        submit.on('click', function () {
            if (response.val()) {
                var answer = response.val();
                answers[questionIndex] = answer;
                // question ++;

                if (questionIndex === questions.length - 1) {
                    pageFlip('forward');
                } else {
                    questionPage.fadeOut(1000, function () {
                        var nextAnswer = answers[questionIndex + 1];

                        if (nextAnswer === '') {
                            submit.css('opacity', '0.15');
                        } else {
                            submit.css('opacity', '1');
                        };

                        questionIndex ++;
                        response.val(nextAnswer);
                        question.text(questions[questionIndex]);
                        questionPage.fadeIn(1000);
                    });
                }
            }
        });
    }

    // ------------------------------------ //

    function populateGallery (callback) {
        $.getJSON('db/animals.json')
        .done(function (json) {
            console.log("Success!");
            console.log(json);
            animalsDB = json;

            // create selection button for each animal:
            $.each(animalsDB, function (index, animal) {
                console.log("Name: " + animal.name);
                $('#gallery').append(
                    '<div class="next-button" id="animal-selection">\
                        <img src="'+ animal.image +'" />\
                        <h3>' + animal.name +'</h3>\
                    </div>'
                );
            });

            callback();
        })
        // error case:
        .fail(function (jqhxr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log('Failed!: ' + err);
        });
    }


    ////////////////////////
    // Setup stuff...     //
    ////////////////////////

    // disable right mouse click
    document.addEventListener('contextmenu', event => event.preventDefault());
    // hide all pages
    allPages.hide();
    // hide navbar
    $('#navbar').hide();
    // hide container
    container.hide();
    // populate animal gallery
    populateGallery(function () {
    // initialize buttons
        navigation();
        submitResponse();
    });
    // show splash page
    $('.container .page:nth-child(1)').show();
    // fade in splash
    fadeTransition(container);
});
